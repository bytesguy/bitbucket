//
//  ViewController.swift
//  swift-test
//
//  Created by Adam Hartley on 18/05/2019.
//  Copyright © 2019 Adam Hartley. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonTapped(_ sender: Any) {
        helloLabel.text = "Hello Again!";
    }
    
}

